var server = require("./server4");
var router = require("./router4");
var requestHandlers = require("./requestHandlers4");

var handle = {}

handle["/"] = requestHandlers.inicio;
handle["/inicio"] = requestHandlers.inicio;
handle["/cargar"] = requestHandlers.cargar;

server.start(router.route,handle);
