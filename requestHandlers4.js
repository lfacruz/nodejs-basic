var exec = require("child_process").exec;
var qs = require('querystring');

function inicio(response,postData) {
    console.log("Manipulador de petición 'inicio' fue llamado.");
    
    var body = '<html>'+
    '<head>'+
    '<meta http‐equiv="Content‐Type" content="text/html;charset=UTF‐8" />'+
    '</head>'+
    '<body>'+
    '<form action="/cargar" method="post">'+
    '<label for="producto">Codigo producto</label>'+
    '<input type="text" name="producto" required/>'+
    '<label for="nombre_producto">Descripcion producto</label>'+
    '<input type="text" name="nombre_producto" required/>'+
    '<input type="submit" value="Enviar"/>'+
    '</form>'+
    '</body>'+
    '</html>';
    
    response.writeHead(200, {
    'Content-Length': Buffer.byteLength(body),
    'Content-Type': 'text/html'
    });
    response.write(body);
    response.end();
}

function cargar(response,postData) {
    console.log("Manipulador de petición 'cargar' fue llamado.");
    
    var producto = qs.parse(postData)["producto"];
    var nombre_producto = qs.parse(postData)["nombre_producto"];
    
    var body = '<html>'+
    '<head></head>'+
    '<body><h1>producto '+producto+' con nombre '+nombre_producto+'</h1></body></html>';
    
    response.writeHead(200, {
    'Content-Length': Buffer.byteLength(body),
    'Content-Type': 'text/html'
    });
    response.write(body);
    response.end();
}


exports.inicio = inicio;
exports.cargar = cargar;
 
