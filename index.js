var server = require("./server3");
var router = require("./router2");
var requestHandlers = require("./requestHandlers2");

var handle = {}

handle["/"] = requestHandlers.start;
handle["/start"] = requestHandlers.start;
handle["/subir"] = requestHandlers.subir;
handle["/bajar"] = requestHandlers.bajar;

server.start(router.route,handle);
