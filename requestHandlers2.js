var exec = require("child_process").exec;

function start(response,postData) {
    console.log("Manipulador de petición 'start' fue llamado.");
    
    var body = '<html>'+
    '<head>'+
    '<meta http‐equiv="Content‐Type" content="text/html;charset=UTF‐8" />'+
    '</head>'+
    '<body>'+
    '<form action="/subir" method="post">'+
    '<textarea name="text" rows="20" cols="60"></textarea>'+
    '<input type="submit" value="Enviar texto" />'+
    '</form>'+
    '</body>'+
    '</html>';

    response.writeHead(200, {
    'Content-Length': Buffer.byteLength(body),
    'Content-Type': 'text/html'
    });
    response.write(body);
    response.end();
}

function subir(response,postData) {
    console.log("Manipulador de petición 'subir' fue llamado.");
    
    var body = '<html>'+
    '<head></head>'+
    '<body><h1>Tu enviaste'+postData+'</h1></body></html>';
    
    response.writeHead(200, {
    'Content-Length': Buffer.byteLength(body),
    'Content-Type': 'text/html'
    });
    response.write(body);
    response.end();
}

function bajar(response,postData) {
    console.log("Manipulador de petición 'bajar' fue llamado.");
    
    var body = 'bajar';
    
    response.writeHead(200, {
    'Content-Length': Buffer.byteLength(body),
    'Content-Type': 'text/plain'
    });
    response.write(body);
    response.end();
}

exports.start = start;
exports.subir = subir;
exports.bajar = bajar;
 
