function start() {
    console.log("Manipulador de petición 'iniciar' ha sido llamado.");
    return "Iniciar";
}
function subir() {
    console.log("Manipulador de petición 'subir' ha sido llamado.");
    return "Subir";
}
exports.start = start;
exports.subir = subir;
